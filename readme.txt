Homebrew Cask Bulk Installer (Mac)

Creds:
@kingthor
@link http://lifehacker.com/how-to-make-your-own-bulk-app-installer-for-os-x-1586252163



// Step 1: Install Homebrew and Homebrew Cask

Open terminal

Run:


ruby -e "$(curl -fsSL "https://raw.githubusercontent.com/Homebrew/install/master/install)"


Once complete, run:


brew tap caskroom/cask

brew install caskroom/cask/brew-cask


// Step 2: Locate caskconfig.sh

Locate and move caskconfig.sh into the home directory of the new machine


// Step 3: Run Bulk Installer

In terminal, run:

./caskconfig.sh

// Step 4: Wait